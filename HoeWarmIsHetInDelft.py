from __future__ import print_function  # backward compatability with python2
import urllib.request
import re
import sys
import time
import bs4 as bs  # to parse html pages
from selenium import webdriver
# import Options to pass start arguments to webdriver
from selenium.webdriver.chrome.options import Options


def get_dashboard(url):
    try:
        page = urllib.request.urlopen(url)
    except urllib.error.URLError as err:
        print(err)
        sys.exit(1)
    except ValueError as err:
        print(err)
        sys.exit(1)

    soup = bs.BeautifulSoup(page, 'html.parser')  # parse page

    try:
        # get url of dashboard with temperature
        dashboard = soup.find('iframe', attrs={'id': 'ifrm_3'})['src']
    except TypeError:
        print("Couldn't find iframe with temperature. Only 'https://www.weerindelft.nl/' will work")
        sys.exit(1)

    return dashboard


def get_temp():
    if len(sys.argv) == 1:
        url = "https://www.weerindelft.nl/"
    else:
        url = str(sys.argv[1])
    dashboard = get_dashboard(url)
    chrome_options = Options()
    chrome_options.add_argument("--no-sandbox")  # disable chrome sandbox to prevent crashes
    chrome_options.add_argument("--headless")  # do not open browser
    driver = webdriver.Chrome(options=chrome_options)  # pass options to webdriver
    driver.get(dashboard)
    temp_span = driver.find_element_by_id('ajaxtemp')  # find span with id 'ajaxtemp'
    while temp_span.text == "2.4°C":  # wait for element to be updated
        time.sleep(0.05)
    result = temp_span.text  # get test from span
    result = round(float(re.sub("[^0-9.]", "", result)))  # remove everything except degrees
    return result


if __name__ == "__main__":
    print("% d degrees Celsius" % get_temp())  # format output
