FROM python:3.7-stretch

ENV CHROME_VERSION="77.0.3865.90-1"
ENV CHROMEDRIVER_VERSION="77.0.3865.40"

WORKDIR /root

# download chromedriver
ADD https://chromedriver.storage.googleapis.com/${CHROMEDRIVER_VERSION}/chromedriver_linux64.zip .
RUN unzip chromedriver_linux64.zip && mv chromedriver /usr/local/bin
# add chrome repo
RUN curl -sS -o - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - && \
    echo "deb [arch=amd64]  http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list

# install google chrome and remove apt cache
RUN apt -y update && apt -y install google-chrome-stable=${CHROME_VERSION} && \
    apt clean

COPY requirements .

# install python packages
RUN pip install -r requirements

COPY . .

CMD ["python", "HoeWarmIsHetInDelft.py"]